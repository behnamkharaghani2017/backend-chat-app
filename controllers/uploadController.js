const uploadModel = require("../models/upload");
exports.info = async (req, res) => {
    const result = await uploadModel.info(req.body.userID);
    res.json({
      result: result,
    });
};
exports.upImage = async (req, res) => {
  if (req.files) {
    const filePath =
      `http://localhost:4000/` +
      req.files.image[0].path.slice(7).replace(/\\/g, '/');
      const result = await uploadModel.imageProfile(req.body.userID,filePath);
    await res.json({
      filePath: filePath,
    });
  } else {
    res.status(400).json({
      success: false,
    });
  }
};
exports.upVoice = async (req, res) => {
  if (req.files) {
    const filePath =
      `http://localhost:4000/` +
      req.files.voiceMessage[0].path.slice(7).replace(/\\/g, '/');
    await res.json({
      filePath: filePath,
    });
  } else {
    res.status(400).json({
      success: false,
    });
  }
};

exports.upFiles = async (req, res) => {
  if (req.files) {
    const filePath =
      `http://localhost:4000/` +
      req.files.file[0].path.slice(7).replace(/\\/g, '/');
    await res.json({
      filePath: filePath,
    });
  } else {
    res.status(400).json({
      success: false,
    });
  }
};