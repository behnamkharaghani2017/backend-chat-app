const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const chatSchema = Schema(
  {
    userID1: {
      type: Number,
      required: true,
    },
    userID2: {
      type: Number,
      required: true,
    },
    sender: {
      type: Number,
      required: true,
    },
    receiver: {
      type: Number,
      required: true,
    },
    message: {
      type: String,
      required: true,
    },
    type: {
        type: String,
        enum : ['file', 'msg','img','voice'], 
        required: true,
      },
      seen: {
        type: String,
        enum : ['0', '1','2'], 
        default: '0'
      },
  },
  {
    timestamps: true,
  }
);

module.exports = Chat = mongoose.model("chats", chatSchema);
