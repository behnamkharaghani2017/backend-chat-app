const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const infoSchema = Schema(
  {
    userID: {
      type: Number,
      required: true,
    },
    image: {
      type: String,
      required: false,
    },
    mode: {
      type: Boolean,
      default: false
    },
  },
  {
    timestamps: true,
  }
);

module.exports = Info = mongoose.model("info", infoSchema);
