const express = require("express");
const { createServer } = require("http");
const { Server } = require("socket.io");


const app = express();
const httpServer = createServer(app);
const io = new Server(httpServer, {
  cors: {
    origin: "http://localhost:3000",
    // methods: ["GET", "POST"]
  },
});
app.use(express.static('public'));

require("./middlewares")(app);
require("./routes")(app);



const Chat = require("./db/models/mongoose/chat");
const mdb = require("./db/connections/mongoose");

// const { uploadVoice, uploadFile } = require('./middlewares/upload');


// const upVoice = uploadVoice.fields([{ name: 'voiceMessage', maxCount: 1 }]);
// const upFiles = uploadFile.fields([{ name: 'file', maxCount: 1 }]);

// app.post('/uploadVoice', upVoice, async (req, res) => {
//   if (req.files) {
//     const filePath =
//       `http://localhost:4000/` +
//       req.files.voiceMessage[0].path.slice(7).replace(/\\/g, '/');
//     await res.json({
//       filePath: filePath,
//     });
//   } else {
//     res.status(400).json({
//       success: false,
//     });
//   }
// });
// app.post('/uploadFile', upFiles, async (req, res) => {
//   if (req.files) {
//     const filePath =
//       `http://localhost:4000/` +
//       req.files.file[0].path.slice(7).replace(/\\/g, '/');
//     await res.json({
//       filePath: filePath,
//     });
//   } else {
//     res.status(400).json({
//       success: false,
//     });
//   }
// });


const users = [];
const ids_socket = {};

io.on("connection", (socket) => {
  console.log("Connected: " + socket.userId);

  socket.on("user online", ({ idu, id }) => {
    // console.log("rrrrr", idu, id);
    // users[idu.id] = id;
    ids_socket[idu] = id;
    console.log("eeee111", ids_socket[idu]);
    // console.log("eeee111", io);
    console.log("eeee2", socket.id);
    console.log("eeee3", id);
    io.to(socket.id).emit("onliner", {ids_socket});
    // console.log(`user id is : ${users}`)
    // console.log(`user socket is : ${user.sid}`)
  });
  socket.on("disconnect", () => {
    console.log("Disconnected: " + socket.id);
    const key = Object.keys(ids_socket).find(key => ids_socket[key] === socket.id);
    Object.keys(ids_socket).find(key => console.log('Object.keys',ids_socket[key],socket.id));
    console.log("Disconnected2: " + key);
    io.emit(`disconnect${key}`, {socket:socket.id});
  });
  socket.on("call-user", (data) => {
    socket.to(data.to).emit("call-made", {
      offer: data.offer,
      socket: socket.id,
    });
  });
  socket.on("make-answer", (data) => {
    socket.to(data.to).emit("answer-made", {
      socket: socket.id,
      answer: data.answer,
    });
  });

  socket.on("reject-call", (data) => {
    socket.to(data.from).emit("call-rejected", {
      socket: socket.id,
    });
  });

  socket.on("istyping", ({directId,userMeta}) => {
    io.emit(`istyping${directId}`, {directId,userMeta});
    // socket.to(ids_socket[id]).emit("istyping",'11111111');
    // socket.to(ids_socket[id]).emit("private message", socket.id, msg);

    // console.log('sssdd11',socket.id)
    // console.log('sssdd',ids_socket[id])
    // console.log('sssdd',ids_socket[2])
  });

  socket.on("new chat", ({msg,id,userId}) => {
    const sender = userId.id;
    const receiver = id;
    //save chat to the database
    mdb.then((db) => {
      console.log("connected correctly to the server");
      let chatMessage = new Chat({userID1:sender,userID2:receiver, message: msg,sender,receiver,type:'msg'  });
      chatMessage.save(function(err, doc) {
        if (err) return console.error(err);
        // io.to(`${myUsername}:${username}`).to(`${username}:${myUsername}`).emit("new chat", ({msgs:msg,ids:id,userIds:userId.id,doc}));
        socket.broadcast.emit("new chat", ({msgs:msg,ids:id,userIds:userId.id,doc}));
        console.log("Document inserted succussfully!");
        // console.log(doc);
        // console.log(chatMessage);
      });
    });
  });
  socket.on("seenMessage", ({sender,receiver}) => {
    // console.log(doc)
    // const sender = doc.sender;
    // const receiver = doc.receiver;
    //save chat to the database
    mdb.then((db) => {
      
    //   console.log("connected correctly to the server");
      let chatMessage = new Chat();
    var conditions = { sender:sender,receiver: receiver, } 
    , update = { $set: { seen: 2 }}
    Chat.updateMany(conditions, update, { multi: true }).then(updatedRows=>{
     socket.broadcast.emit("seenMessage", ({updatedRows}));

  }).catch(err=>{
    console.log(err)
    
  })
  // Model.update(conditions, update, { multi: true })
    //   chatMessage.updateMany(function(err, doc) {
    //     if (err) return console.error(err);
    //     // io.to(`${myUsername}:${username}`).to(`${username}:${myUsername}`).emit("new chat", ({msgs:msg,ids:id,userIds:userId.id,doc}));
    //     socket.broadcast.emit("new chat", ({msgs:msg,ids:id,userIds:userId.id,doc}));
    //     console.log("Document inserted succussfully!");
    //     // console.log(doc);
    //     // console.log(chatMessage);
    //   });
    });
  });


  socket.on('uploadVoice', ({ path, sender, receiver }) => {
    const myUsername = sender.name;
    const username = receiver.name;
    mdb.then((db) => {
      console.log("connected correctly to the server");
      let chatMessage = new Chat({userID1:sender.name,userID2:receiver.name, message: path, sender: sender.name,receiver:receiver.name,type:'voice' });
      chatMessage.save(function(err, doc) {
        if (err) return console.error(err);
        socket.broadcast.emit("new chat", ({msgs:path,ids:receiver.name,userIds:sender.name,doc}));
        console.log("Document inserted succussfully!");
        // console.log(doc);
        // console.log(chatMessage);
        // io
        // .to(`${myUsername}:${username}`)
        // .to(`${username}:${myUsername}`)
        // .emit('newMessage', {
        //   type: 'voice',
        //   sender,
        //   receiver,
        //   date: new Date(),
        //   path,
        //   id: Math.floor(Math.random() * Math.pow(10, 7)),
        // });
      });
    });
  
  });
  socket.on('uploadFile', ({ path, sender, receiver }) => {
    const myUsername = sender.name;
    const username = receiver.name;
    io
      .to(`${myUsername}:${username}`)
      .to(`${username}:${myUsername}`)
      .emit('newMessage', {
        type: 'file',
        sender,
        receiver,
        date: new Date(),
        path,
        id: Math.floor(Math.random() * Math.pow(10, 7)),
      });
  });
  socket.on('direct online',({ sender, receiver})=>{
    io.emit(`direct online${sender}`,{sender,receiver})
    // io.emit(`direct online${receiver}`,{sender,receiver})
    console.log(sender,receiver)
    console.log(`direct online${sender}`)
  })
  socket.on('direct online answer',({ sender, receiver})=>{
    io.emit(`direct online answer${sender}`,{sender,receiver})
    // io.emit(`direct online${receiver}`,{sender,receiver})
    // console.log(sender,receiver)
    // console.log(`direct online${sender}`)
  })
});

// const sms = require('./app/services/sms');
httpServer.listen("4000", (port) => {
  // sms.post();
  console.log(`behnam is connected : ${port}`);
});
