const dashModel = require("../models/dash");

exports.pv = async (req, res) => {
  const params = req.body;
  const [users, chats] = await dashModel.pv(params.id, params.pvId);
  return res.send([users, chats]);
  // return res.send(params.id)
};
exports.users = async (req, res) => {
  const users = await dashModel.users();
  return res.json(users);
};
exports.user = async (req, res) => {
  const user = await dashModel.user(req.body);
  // return res.json(user.id ? true : false);
  return res.json(user)
};
exports.settings = async (req, res) => {
  console.log("files", req.files.image);
  if (req.files.image != undefined) {
    const filePath =
      `http://localhost:4000/` +
      req.files.image[0].path.slice(7).replace(/\\/g, "/");
    req.body.image = filePath;
    // const result = await uploadModel.imageProfile(req.body.userID,filePath);
  }
  const settings = await dashModel.settings(req.body);
  return res.json(settings);
};
