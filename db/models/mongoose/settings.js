const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const settingsSchema = Schema(
  {
    userID: {
      type: Number,
      required: true,
    },
    image: {
      type: String,
      required: false,
    },
    mode: {
      type: Boolean,
      default: false,
    },
    firstName: {
      type: String,
    },
    lastName: {
      type: String,
    },
    mobile: {
      type: String,
    },
    email: {
      type: String,
    },
    bio: {
        type: String,
      },
  },
  {
    timestamps: true,
  }
);

module.exports = settings = mongoose.model("settings", settingsSchema);
