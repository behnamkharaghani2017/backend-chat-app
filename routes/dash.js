const express = require("express");
const router = express.Router();
const dashController = require("../controllers/dashController");
const { uploadVoice, uploadFile,uploadImg } = require('../middlewares/upload');

const upImg = uploadImg.fields([{ name: 'image', maxCount: 1 }]);
router.post("/pv", dashController.pv);
router.get("/users", dashController.users);
router.post("/user", dashController.user);
router.post("/settings",upImg, dashController.settings);

module.exports = router;
