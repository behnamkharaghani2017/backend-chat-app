const mongoose = require('mongoose');

const mdb = mongoose.connect('mongodb://localhost:27017/chat_app')
        .then(()=>console.log('connected mongoose'))
        .catch((err)=>console.log(err));

module.exports = mdb