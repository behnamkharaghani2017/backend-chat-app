const authRouter = require("./auth");
const dashRouter = require("./dash");
const uploadRouter = require("./upload");

module.exports = (app) => {
  app.use("/api/v1/auth", authRouter);
  app.use("/api/v1", dashRouter);
  app.use("/api/v1", uploadRouter);
};
