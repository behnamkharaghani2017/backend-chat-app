const db = require("../db/connections/sequelize");
// const mdb = require('../db/connections/mongoose');
const Chat = require("../db/models/mongoose/chat");
const Settings = require("../db/models/mongoose/settings");
// const sms = require('../../services/sms');
exports.pv = async (id, pvId) => {
  const userID1 = id;
  const userID2 = pvId;
  // let chats;
  // const id = params;
  // const code = Math.floor((Math.random() * 99999) + 10000);
  // const code = Math.floor(Math.random() * (99999 - 10000 + 1) + 10000);
  // const user = await db.User.create(params);
  const user = await db.User.findOne({ where: { id } });
  // mdb.then(db => {
  // chats = mdb.find({});
  // Chat.find({}).then(chat => {
  //  return chat;
  // });
  // mdb.find({}).then(chat => {
  //   res.json(chat);
  // });
  // });
  // const chats = await mdb.findAll
  // .then(()=>'is conn').catch(err=>'err')
  const chats = await Chat.find({
    //   $and: [
    //     { $or: [{a: 1}, {b: 1}] },
    //     { $or: [{c: 1}, {d: 1}] }
    // ]
    $or: [
      {
        $and: [{ userID1: userID1 }, { userID2: userID2 }],
      },
      {
        $and: [{ userID1: userID2 }, { userID2: userID1 }],
      },
    ],
  });

  // ['William Riker']
  chats.map((chat) => chat.createdAt).sort();
  // const chat = await Chat.find({ userID1, userID2 }).sort({ "createdAt" : -1 })
  //   .skip(offset).limit(limit)
  // const user = await db.User.findAll({where:{mobile}});
  // const user = await db.User.findAndCountAll({where:{mobile}});
  // const [user,status] = await db.User.findOrCreate({where:{mobile}});
  // const [update] = await db.User.update({code },{where:{id:user.id}});
  // const sent = await sms.sendSms(code,mobile);
  return [user, chats];
};
exports.users = async (req, res) => {
  const users = await db.User.findAll();
  console.log(users);
  return users;
};
exports.user = async (params) => {
  const user = await db.User.findOne({
    where: { token: params.token, id: params.userId },
  });
  return user;
};
exports.settings = async (params) => {
  const item = await Settings.findOne({ userID: params.userID }).exec();
  console.log('params',params)
  let obj = {};

  params.image ? obj.image = params.image: '' ;
  params.mode ? obj.mode = params.mode: '' ;
  if(item){
    // console.log('111',item)
        const update = await Settings.updateOne({_id:item._id},obj)
        // return update;
  }else{
    // console.log('2222',item)
      let settings = new Settings({userID:params.userID,image:params.image });
      settings.save(function(err, doc) {
        if (err) return console.error(err);
        // return doc;
      })
  }
  const newSettings = await Settings.findOne({ userID: params.userID }).exec();

  // const settings = await Settings.User.findAll();
  // console.log('newSettings',newSettings);
  return newSettings;
};
