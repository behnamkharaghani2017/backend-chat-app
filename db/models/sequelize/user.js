const Sequelize = require('sequelize');

module.exports = (sequelize,DataTypes)=>{
    return sequelize.define('users', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey:true,
            autoIncrement: true
          },
        firstName: {
          type: Sequelize.STRING,
          allowNull: true
        },
        lastName: {
          type: Sequelize.STRING
        },
        mobile:{
            type:Sequelize.STRING(11)
        },
        token:{
            type:Sequelize.STRING
        },
        code:{
            type:Sequelize.STRING(5)
        },
        expire_code:{
            type:Sequelize.STRING
        },
        created_at:{
            type:Sequelize.DATE,
            defaultValue:Sequelize.NOW,
        }
      }, {
          freezeTableName:true,
          TableName:'users'
      });      
} 