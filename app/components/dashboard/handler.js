const dashService = require('./dashService');

exports.pv = async (req,res)=>{
    const params = req.body;
    const [users,chat] = await dashService.pv(params.id)
    return res.send([users,chat])
    // return res.send(params.id)
}
exports.users = async (req,res)=>{
    const users = await dashService.users()
    return res.json(users )
}
