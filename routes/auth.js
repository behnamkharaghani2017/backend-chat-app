const express = require('express');
const router = express.Router();
const authController = require('../controllers/authController');;

router.post('/login',authController.login)
router.post('/verify',authController.verify)
router.post('/checktoken',authController.checktoken)

module.exports = router; 