const authModel = require("../models/auth");
class authController {
  login = async (req, res) => {
    const newUserData = req.body;
    const newUser = await authModel.login(newUserData);
    return res.send({ newUser });
  };

  verify = async (req, res) => {
    const newUserData = req.body;
    const newUser = await authModel.verify(newUserData);
    if (newUser == null) {
      return res.status(400).send({
        success: false,
        message: "sssssss",
      });
    }
    // const token = token.sign({uid:newUser.id});
    const token = "110ec58a-a0f2-4ac4-8393-c866d813b8d1sss";

    const user = await authModel.setLocalStorage(newUser, token);
    return res.send({
      success: true,
      token,
      user,
    });
  };

  checktoken = async (req, res) => {
    const id = req.body;
    const user = await authModel.checktoken(id);
    return res.send({
      success: true,
      user,
    });
  };
}
module.exports = new authController();
