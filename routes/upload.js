const express = require("express");
const router = express.Router();
const uploadController = require("../controllers/uploadController");
const { uploadVoice, uploadFile,uploadImg } = require('../middlewares/upload');
const upVoice = uploadVoice.fields([{ name: 'voiceMessage', maxCount: 1 }]);
const upFiles = uploadFile.fields([{ name: 'file', maxCount: 1 }]);
const upImg = uploadImg.fields([{ name: 'image', maxCount: 1 }]);
// const multer  = require('multer');

// const upload = multer({ dest: './public/uploads/' })
router.post("/info", uploadController.info);
router.post("/uploadImage",upImg, uploadController.upImage);
router.post("/uploadVoice", upVoice,uploadController.upVoice);
router.post("/uploadFile", upFiles,uploadController.upFiles);

module.exports = router;