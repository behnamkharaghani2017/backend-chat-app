const { v4: uuidv4 } = require("uuid");

exports.generateUuid = () => {
    return uuidv4();
}
   