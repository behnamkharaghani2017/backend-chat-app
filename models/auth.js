const db = require("../db/connections/sequelize");
// const sms = require('../../services/sms');
class authModel {
  login = async (params) => {
    const mobile = params.mobile;
    // const code = Math.floor((Math.random() * 99999) + 10000);
    const code = Math.floor(Math.random() * (99999 - 10000 + 1) + 10000);
    // const user = await db.User.create(params);
    // const user = await db.User.findOne({where:{mobile}});
    // const user = await db.User.findAll({where:{mobile}});
    // const user = await db.User.findAndCountAll({where:{mobile}});
    const [user, status] = await db.User.findOrCreate({ where: { mobile } });
    const [update] = await db.User.update({ code }, { where: { id: user.id } });
    const newUser= await db.User.findOne({ where: { mobile } });
    // const sent = await sms.sendSms(code,mobile);
    return newUser;
  };

  verify = async (params) => {
    const user = await db.User.findOne({
      where: { mobile: params.mobile, code: params.code },
    });
    return user;
  };
  setLocalStorage = async (user, token) => {
    const [update] = await db.User.update(
      { token },
      { where: { id: user.id } }
    );
    return user.id;
  };
  checktoken = async (id) => {
    const user = await db.User.findByPk(id.userd);
    return user;
  };
}
module.exports = new authModel();
