const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('chat_app', 'root', '', {
    host: 'localhost',
    dialect: 'mysql',
    define:{
        timestamps:false
    },
    logging: false, 
    timezone:"+03:30"
  });
  const db = {};
sequelize.authenticate().then(()=>{
    console.log('Connection has been established successfully.');
}).catch((err)=>{
    console.error('Unable to connect to the database:', err);
})
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.User= require('../models/sequelize/user.js')(sequelize, Sequelize);
// db.User = sequelize.import('../models/sequelize/user.js')
db.sequelize.sync();
module.exports = db;