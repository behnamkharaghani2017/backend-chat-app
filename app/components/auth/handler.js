const authService = require('./authService');
const token = require('../../services/token');

exports.login = async (req,res)=>{
    const newUserData = req.body;
    const newUser = await authService.login(newUserData)
    return res.send({newUser})
}

exports.verify = async (req,res)=>{
    const newUserData = req.body;
    const newUser = await authService.verify(newUserData)
    if( newUser==null ){
        return res.status(400).send({
            success:false,
            message:'sssssss'
        })
    }
    // const token = token.sign({uid:newUser.id});
    const token = '110ec58a-a0f2-4ac4-8393-c866d813b8d1sss';

    const user = await authService.setLocalStorage(newUser,token)
    return res.send({
        success:true,
        token,
        user
    });
}

exports.checktoken = async (req,res)=>{
    const id = req.body;
    const user = await authService.checktoken(id)
    return res.send({
        success:true,
        user
    });
}